<h1 align="center">Hi 👋, I'm Sanskar</h1>
<h3 align="center">An Android and Desktop Native developer</h3>

<p align="left"> <a href="https://github.com/ryo-ma/github-profile-trophy"><img src="https://github-profile-trophy.vercel.app/?username=sanskar10100" alt="sanskar10100" /></a> </p>

<p align="left"> <a href="https://twitter.com/sanskar10100" target="blank"><img src="https://img.shields.io/twitter/follow/sanskar10100?logo=twitter&style=for-the-badge" alt="sanskar10100" /></a> </p>

- 🔭 I’m currently working on [Ashwini](https://github.com/AshwiniApp/Ashwini)

- 🌱 I’m currently learning **Kotlin**

- 👨‍💻 All of my projects are available at [my website](https://sanskar10100.github.io)

- 💬 Ask me about **Programming in general 😅️**

- 📫 Reach me at **sanskar10100@gmail.com**

- 📄 Know about my experiences [here](https://drive.google.com/file/d/1PnVVGjkfPv_QU4B6ruKobaDbh0rEKSYj/view?usp=sharing)

- ⚡ Fun fact **I pretend I'm a good gamer (I'm not)**

<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://twitter.com/sanskar10100" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/twitter.svg" alt="sanskar10100" height="30" width="40" /></a>
<a href="https://linkedin.com/in/sanskar10100" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="sanskar10100" height="30" width="40" /></a>
<a href="https://stackoverflow.com/users/6315197" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/stack-overflow.svg" alt="6315197" height="30" width="40" /></a>
<a href="https://www.codechef.com/users/sanskar10100" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.1.0/icons/codechef.svg" alt="sanskar10100" height="30" width="40" /></a>
<a href="https://www.hackerrank.com/sanskar10100" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/hackerrank.svg" alt="sanskar10100" height="30" width="40" /></a>
<a href="https://codeforces.com/profile/sanskar10100" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/codeforces.svg" alt="sanskar10100" height="30" width="40" /></a>
<a href="https://www.leetcode.com/sanskar10100" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/leet-code.svg" alt="sanskar10100" height="30" width="40" /></a>
</p>

<h3 align="left">Languages and Tools:</h3>
<p align="left"> <a href="https://developer.android.com" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/android/android-original-wordmark.svg" alt="android" width="40" height="40"/> </a> <a href="https://www.cprogramming.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" width="40" height="40"/> </a> <a href="https://www.w3schools.com/cpp/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" width="40" height="40"/> </a> <a href="https://firebase.google.com/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/firebase/firebase-icon.svg" alt="firebase" width="40" height="40"/> </a> <a href="https://git-scm.com/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> <a href="https://www.java.com" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="40" height="40"/> </a> <a href="https://kotlinlang.org" target="_blank"> <img src="https://www.vectorlogo.zone/logos/kotlinlang/kotlinlang-icon.svg" alt="kotlin" width="40" height="40"/> </a> <a href="https://www.linux.org/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> </a> <a href="https://www.python.org" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a> <a href="https://www.sqlite.org/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/sqlite/sqlite-icon.svg" alt="sqlite" width="40" height="40"/> </a> </p>

<p><img align="left" src="https://github-readme-stats.vercel.app/api/top-langs?username=sanskar10100&show_icons=true&theme=radical&locale=en&layout=compact" alt="sanskar10100" /></p>

<p>&nbsp;<img align="center" src="https://github-readme-stats.vercel.app/api?username=sanskar10100&show_icons=true&theme=dark&locale=en" alt="sanskar10100" /></p>

<p><img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=sanskar10100&" alt="sanskar10100" /></p>
